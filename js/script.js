function loadDoc() {
	var url = document.getElementById('url').value;
	var responseParse = getGeolocation(url);
	document.getElementById("GeoInfo").innerHTML = createGeoList(responseParse); // <-- Put whatever you want done next here
}

function getGeolocation(url) {
	var xhttp;

	url = "http://ip-api.com/json/".concat(url);

	if (window.XMLHttpRequest) {
		// Code for all modern browsers
	   xhttp = new XMLHttpRequest();
   } else {
	   // Code for IE6, IE5 (dinosaurs)
	   xhttp = new ActiveXObject("Microsoft.XMLHTTP");
	}

	xhttp.onreadystatechange = function() {
	   if (xhttp.readyState == 4 && xhttp.status == 200) {
			responseParse = JSON.parse(xhttp.responseText);
		}
	};

	// Use a GET request to send a synchronous (async = false) request to the API
	// Note: We know that the synchronous method is deprecated, but will refactor later
	xhttp.open("GET", url, false);
	xhttp.send();

	return responseParse;
}

function createGeoList(responseParse) {
	var tableList = "STATUS: " + responseParse.status 
	+ "<br>IP ADDRESS USED FOR QUERY: " + responseParse.query
	+ "<br>ORGANIZATION NAME: " + responseParse.org 
	+ "<br>ISP NAME: " + responseParse.isp 
	+ "<br>AS NUMBER / NAME: " + responseParse.as
	+ "<br>CITY: " + responseParse.city 
	+ "<br>REGION NAME: " + responseParse.regionName 
	+ "<br>REGION: " + responseParse.region 
	+ "<br>COUNTRY: " + responseParse.country 
	+ "<br>COUNTRY CODE: " + responseParse.countryCode 
	+ "<br>ZIP CODE: " + responseParse.zip 
	+ "<br>TIME ZONE: " + responseParse.timezone 
	+ "<br>LATITUDE: " + responseParse.lat 
	+ "<br>LONGITUDE: " + responseParse.lon
	+" <br><br>";

	return tableList;
}

function createTestJSON() {
	var testGeoArray = [];
	var urlList = [
		"184.17.239.232", 
		"184.18.128.1", 
		"104.169.205.237", 
		"74.40.1.33",
		"74.40.2.205",
		"74.40.4.138",
		"206.223.119.37",
		"184.105.222.173",
		"72.52.92.114",
		"72.52.93.38",
		"203.221.3.7",
		"203.219.107.198",
		"203.10.14.26",
		"202.10.13.181",
		"192.189.53.33"
	];

	// Set the innerHTML to nothing so we can append later
	for (var i = 0; i < urlList.length; i++) {
		var testGeoResult = getGeolocation(urlList[i])
		testGeoArray = testGeoArray.concat(testGeoResult);
		document.getElementById("GeoInfoJSON").innerHTML += createGeoList(testGeoResult);
	}

	return testGeoArray;
}